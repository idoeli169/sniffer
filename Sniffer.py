from scapy.all import*
import sys

WEATHER_SERVER_IP = "52.41.152.51" # This server is not up anymore

def main():
    print("\nWelcome to Ido Elimelech's Sniffer!")
    option = 1
    
    while(0 < option < 4):
        option = menu()
        
        if (option == 1):
            dns_domains()
            
        elif (option == 2):
            weather_answers()
            
        elif (option == 3):
            get_addresses()
        
        else:
            print("Bye Bye! :)")

def menu():
    print("\n1. print DNS domains")
    print("2. print answer from the weatherClient")
    print("3. print GET adresses")
    print("Press any other number to exit...")
    try:
        option = int(input("Please choose an option: "))
    except Exception as e:
        print("Error! Please try again!")
        menu()
    
    return option

def get_addresses():
    # Sniffing and looking for GET requests
    try:
        packets = sniff(lfilter=is_ok, prn = print_get_addresses)
    except Exception as e:
        print(e)
        
def print_get_addresses(packet):
    packet = str(packet)
    packet = packet.split("GET")[1] # splits "GET" and takes the side with the address
    packet = packet.split("HTTP")[0]# splits again but "HTTP" this time and take the side with the address
    print(packet)                   # because GET request is built up that way: "GET /address/ HTTP/1.1"
                                    # so I take the part with the address everytime until I isolate it
    
def is_ok(packet):
    # Filters GET requests
    return ((Raw in packet) and (b"GET" in packet[Raw].load))

def weather_answers():
    # Sniffing and looking for answers that came from the Weather server 
    try:
        packets = sniff(lfilter=is_weather, prn = print_weather_answers)
    except Exception as e:
        print(e)
        
def print_weather_answers(packet):
    # Printing only the answers that came from the weather server
    if(b"ANSWER" in packet[Raw].load):
        packet = str(packet[Raw]).split("'")[1]
        print(packet)
        
def is_weather(packet):
    # Filters for only packets from the weather server
    return ((Raw in packet) and (packet[IP].src==WEATHER_SERVER_IP))

def dns_domains():
    # Sniffing dns packets
    packets = sniff(lfilter=is_dns, prn = print_dns_domains)
        
def print_dns_domains(packet):
    print("")
    # Prints the domain
    packet_name = str(packet[DNSQR].qname)
    packet_name = packet_name.split("'")[1]
    print(packet_name)

def is_dns(packet):
    # returns if it's a DNS packet
    return DNS in packet


if __name__ == "__main__":
    main()
